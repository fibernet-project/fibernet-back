FROM eclipse-temurin:21-jdk-alpine

LABEL authors="Salih GUERMOUD"

COPY target/fibernet-1.0.0.jar fibernet.jar

ENTRYPOINT ["java", "-jar", "fibernet.jar", "--host=0.0.0.0"]