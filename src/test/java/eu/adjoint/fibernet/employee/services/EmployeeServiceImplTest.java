package eu.adjoint.fibernet.employee.services;

import eu.adjoint.fibernet.employee.models.entities.Employee;
import eu.adjoint.fibernet.employee.models.repositories.EmployeeRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class) // To activate Mockito with JUnit and initialize Mockito fields
class EmployeeServiceImplTest {

    @Mock // It's creates a mock with this field
    private EmployeeRepository employeeRepository;

    @InjectMocks // To inject our mock into this service class
    private EmployeeServiceImpl employeeService;

    @Test
    void getAll_shouldReturnEmployeeList() {

        // given
        List<Employee> expectedEmployees = new ArrayList<>();
        expectedEmployees.add(new Employee());
        when(employeeRepository.findAll()).thenReturn(expectedEmployees);

        // when
        List<Employee> actualEmployees = employeeService.getAll();

        // then
        assertEquals(expectedEmployees, actualEmployees);
    }

    @Test
    void getById_shouldReturnEmployeeWhenExists() {

        // given
        Long employeeId = 1L;
        Employee expectedEmployee = new Employee();
        when(employeeRepository.findById(employeeId)).thenReturn(Optional.of(expectedEmployee));

        // when
        Optional<Employee> actualEmployee = employeeService.getById(employeeId);

        // then
        assertTrue(actualEmployee.isPresent());
        assertEquals(expectedEmployee, actualEmployee.get());
    }

    @Test
    void getById_shouldReturnEmptyOptionalWhenNotExists() {

        // given
        Long employeeId = 1L;
        when(employeeRepository.findById(employeeId)).thenReturn(Optional.empty());

        // when
        Optional<Employee> actualEmployee = employeeService.getById(employeeId);

        // then
        assertTrue(actualEmployee.isEmpty());
    }

    @Test
    void save_shouldReturnSavedEmployee() {

        // given
        Employee employeeToSave = new Employee();
        when(employeeRepository.save(any(Employee.class))).thenReturn(employeeToSave);

        // when
        Employee savedEmployee = employeeService.save(employeeToSave);

        // then
        assertNotNull(savedEmployee);
        assertEquals(employeeToSave, savedEmployee);
    }

    @Test
    void deleteById_shouldInvokeRepositoryDeleteById() {

        // given
        Long employeeId = 1L;

        // when
        employeeService.deleteById(employeeId);

        // then
        verify(employeeRepository, times(1)).deleteById(employeeId);
    }

    @Test
    void deleteById_shouldReturnNullAfterDeletion() {

        // given
        Long employeeId = 1L;

        // when
        employeeService.deleteById(employeeId);
        Optional<Employee> deletedEmployee = employeeService.getById(employeeId);

        // then
        assertTrue(deletedEmployee.isEmpty());
    }

}
