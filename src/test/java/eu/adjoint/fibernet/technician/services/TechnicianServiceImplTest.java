package eu.adjoint.fibernet.technician.services;

import eu.adjoint.fibernet.technician.models.entities.Technician;
import eu.adjoint.fibernet.technician.models.repositories.TechnicianRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class TechnicianServiceImplTest {
    @Mock
    TechnicianRepository technicianRepository;

    @InjectMocks
    TechnicianServiceImpl technicianService;

    @Test
    void getAll_shouldReturnTechnianList() {

        // given
        List<Technician> expectedTechnicians = new ArrayList<>();
        expectedTechnicians.add(new Technician());
        when(technicianRepository.findAll()).thenReturn(expectedTechnicians);

        // when
        List<Technician> actualTechnicians = technicianService.getAll();

        // then
        assertEquals(expectedTechnicians, actualTechnicians);
    }

    @Test
    void getById_shouldReturnTechnicanWhenExists() {

        // given
        Long technicianId = 1L;
        Technician expectedTechnician = new Technician();
        when(technicianRepository.findById(technicianId)).thenReturn(Optional.of(expectedTechnician));

        // when
        Optional<Technician> acutalTechnician = technicianService.getById(technicianId);

        // then
        assertTrue(acutalTechnician.isPresent());
        assertEquals(expectedTechnician, acutalTechnician.get());
    }

    @Test
    void getById_shouldReturnEmptyOptionalWhenNotExists() {

        // given
        Long technicanId = 1L;
        when(technicianRepository.findById(technicanId)).thenReturn(Optional.empty());

        // when
        Optional<Technician> actualTechncian = technicianService.getById(technicanId);

        // then
        assertTrue(actualTechncian.isEmpty());
    }

    @Test
    void save_shouldReturnSavedTechncian() {

        // given
        Technician technicianToSave = new Technician();
        when(technicianRepository.save(any(Technician.class))).thenReturn(technicianToSave);

        // when
        Technician savedTechnician = technicianService.save(technicianToSave);

        // then
        assertNotNull(savedTechnician);
        assertEquals(technicianToSave, savedTechnician);
    }

    @Test
    void deleteBydId_shouldInvokeRespositoryDeleteById() {

        // given
        Long technicianId = 1L;

        // when
        technicianService.deleteById(technicianId);

        // then
        verify(technicianRepository, times(1)).deleteById(technicianId);
    }

    @Test
    void deleteById_shouldReturnNullAfterDeletion() {

        //given
        Long technicianId = 1L;

        // when
        technicianService.deleteById(technicianId);
        Optional<Technician> deletedTechnician = technicianService.getById(technicianId);

        // then
        assertTrue(deletedTechnician.isEmpty());
    }


}