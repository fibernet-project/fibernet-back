package eu.adjoint.fibernet.contractor.services;

import eu.adjoint.fibernet.contractor.models.entities.Contractor;
import eu.adjoint.fibernet.contractor.models.entities.ContractorInformations;
import eu.adjoint.fibernet.contractor.models.repositories.ContractorRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ContractorServiceImplTest {
    @Mock
    private ContractorRepository contractorRepository;
    @InjectMocks
    private ContractorServiceImpl contractorService;

    @Test
    void getAll_shouldReturnContractorList() {

        // given
        List<Contractor> expectedContractors = new ArrayList<>();
        expectedContractors.add(new Contractor());
        when(contractorRepository.findAll()).thenReturn(expectedContractors);

        // when
        List<Contractor> actualContractors = contractorService.getAll();

        // then
        assertEquals(expectedContractors, actualContractors);
    }

    @Test
    void getById_shouldReturnContractorWhenExists() {

        // given
        Long contractorId = 1L;
        Contractor expectedContractor = new Contractor();
        when(contractorRepository.findById(contractorId)).thenReturn(Optional.of(expectedContractor));

        // when
        Optional<Contractor> actualContractor = contractorService.getById(contractorId);

        // then
        assertTrue(actualContractor.isPresent());
        assertEquals(expectedContractor, actualContractor.get());
    }

    @Test
    void getById_shouldReturnEmptyOptionalWhenExists() {

        // given
        Long contractorId = 1L;
        when(contractorRepository.findById(contractorId)).thenReturn(Optional.empty());

        // when
        Optional<Contractor> actualContractor = contractorService.getById(contractorId);

        // then
        assertTrue(actualContractor.isEmpty());
    }

    @Test
    void save_shouldReturnSavedContractor() {

        // given
        Contractor contractorToSave = new Contractor();

        ContractorInformations contractorInformations = new ContractorInformations();

        Date startDate = new Date(124, 2, 15);
        Date endDate = new Date(124, 2, 30);
        contractorInformations.setKbisStartDate(startDate);
        contractorInformations.setKBisEndDate(endDate);
        contractorInformations.setUrssafStartDate(startDate);
        contractorInformations.setUrssafEndDate(endDate);
        contractorInformations.setDecennaleStartDate(startDate);
        contractorInformations.setDecennaleEndDate(endDate);
        contractorInformations.setForeignWorkersStartDate(startDate);
        contractorInformations.setForeignWorkersEndDate(endDate);
        contractorInformations.setTaxRegularityCertificateStart(startDate);
        contractorInformations.setTaxRegularityCertificateEnd(endDate);

        contractorToSave.setContractorInformations(contractorInformations);

        when(contractorRepository.save(any(Contractor.class))).thenReturn(contractorToSave);

        // when
        Contractor savedContractor = contractorService.save(contractorToSave);

        // then
        assertNotNull(savedContractor);
        assertEquals(contractorToSave, savedContractor);
    }

    @Test
    void deleteById_shouldInvokeRepositoryDeleteById() {

        // given
        Long contractorId = 1L;

        // when
        contractorService.deleteById(contractorId);

        // then
        verify(contractorRepository, times(1)).deleteById(contractorId);
    }

    @Test
    void deleteById_shouldReturnNullAfterDeletion() {

        // given
        Long contractorId = 1L;

        // when
        contractorService.deleteById(contractorId);
        Optional<Contractor> deletedContractor = contractorService.getById(contractorId);

        // then
        assertTrue(deletedContractor.isEmpty());
    }

}