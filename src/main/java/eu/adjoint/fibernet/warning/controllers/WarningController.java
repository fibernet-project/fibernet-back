package eu.adjoint.fibernet.warning.controllers;

import eu.adjoint.fibernet.shared.controllers.BaseController;
import eu.adjoint.fibernet.shared.services.BaseService;
import eu.adjoint.fibernet.warning.models.entities.Warning;
import eu.adjoint.fibernet.warning.services.WarningService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/warnings")
public class WarningController extends BaseController<Warning, Long> {

    private final WarningService warningService;

    public WarningController(BaseService<Warning, Long> service, WarningService warningService) {
        super(service);
        this.warningService = warningService;
    }

    @PutMapping("/{id}")
    public ResponseEntity<Warning> updateWarning(@PathVariable Long id, @RequestBody Warning warningDetails) {
        try {
            warningService.update(id, warningDetails);
            return ResponseEntity.ok(warningDetails);

        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
