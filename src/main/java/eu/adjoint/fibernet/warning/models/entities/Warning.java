package eu.adjoint.fibernet.warning.models.entities;

import eu.adjoint.fibernet.shared.models.entities.Operations;
import jakarta.persistence.*;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

@EqualsAndHashCode(callSuper = true)
@Data
@Table(name = "warning")
@Entity
public class Warning extends Operations implements Serializable {

    private int number;
    private String reason;
}
