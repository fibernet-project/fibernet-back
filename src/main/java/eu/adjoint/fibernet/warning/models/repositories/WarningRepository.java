package eu.adjoint.fibernet.warning.models.repositories;

import eu.adjoint.fibernet.shared.models.repositories.BaseRepository;
import eu.adjoint.fibernet.warning.models.entities.Warning;

public interface WarningRepository extends BaseRepository<Warning, Long> {
}
