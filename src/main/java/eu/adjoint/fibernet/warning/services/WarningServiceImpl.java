package eu.adjoint.fibernet.warning.services;

import eu.adjoint.fibernet.warning.models.entities.Warning;
import eu.adjoint.fibernet.warning.models.repositories.WarningRepository;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
@Transactional
@Service
public class WarningServiceImpl implements WarningService {

    private final WarningRepository warningRepository;

    public WarningServiceImpl(WarningRepository warningRepository) {
        this.warningRepository = warningRepository;
    }

    @Override
    public List<Warning> getAll() {
        return warningRepository.findAll();
    }

    @Override
    public Optional<Warning> getById(Long id) {
        return warningRepository.findById(id);
    }

    @Override
    public Warning save(Warning entity) {
        return warningRepository.save(entity);
    }

    @Override
    public void deleteById(Long id) {
        warningRepository.deleteById(id);
    }

    @Override
    public Warning update(Long id, Warning entity) {
        Warning currentWarning = warningRepository.findById(id).orElseThrow(() ->
                new EntityNotFoundException("Aucun avertissement ne possède l'id : " + id));

        currentWarning.setReason(entity.getReason());

        return warningRepository.save(currentWarning);
    }
}
