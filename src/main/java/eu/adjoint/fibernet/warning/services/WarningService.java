package eu.adjoint.fibernet.warning.services;

import eu.adjoint.fibernet.shared.services.BaseService;
import eu.adjoint.fibernet.warning.models.entities.Warning;

public interface WarningService extends BaseService<Warning, Long> {
}
