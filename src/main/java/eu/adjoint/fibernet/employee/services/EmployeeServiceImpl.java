package eu.adjoint.fibernet.employee.services;

import eu.adjoint.fibernet.employee.models.entities.Employee;
import eu.adjoint.fibernet.employee.models.entities.EmployeeDTO;
import eu.adjoint.fibernet.employee.models.entities.EmployeeMapper;
import eu.adjoint.fibernet.employee.models.repositories.EmployeeRepository;
import eu.adjoint.fibernet.shared.models.entities.Active;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Transactional
@Service
public class EmployeeServiceImpl implements EmployeeService {
    private final EmployeeRepository employeeRepository;
    private final EmployeeMapper employeeMapper;
    @Autowired
    public EmployeeServiceImpl(EmployeeRepository employeeRepository, EmployeeMapper employeeMapper) {
        this.employeeRepository = employeeRepository;
        this.employeeMapper = employeeMapper;
    }

    @Override
    public List<Employee> getAll() {
        return employeeRepository.findAll();
    }

    @Override
    public Optional<Employee> getById(Long id) {
        return employeeRepository.findById(id);
    }

    @Override
    public Employee save(Employee entity) {
        return employeeRepository.save(entity);
    }

    @Override
    public void deleteById(Long id) {
        employeeRepository.deleteById(id);
    }

    @Override
    public Employee update(Long id, Employee entity) {
        Employee currentEmployee = employeeRepository.findById(id).orElseThrow(() ->
                new EntityNotFoundException("Aucun employé ne possède l'id : " + id));


        currentEmployee.setFirstName(entity.getFirstName());
        currentEmployee.setLastName(entity.getLastName());
        currentEmployee.setBirthDate(entity.getBirthDate());
        currentEmployee.setEntryDate(entity.getEntryDate());
        currentEmployee.setHasCompanyMutual(entity.isHasCompanyMutual());
        currentEmployee.setMutualType(entity.getMutualType());
        currentEmployee.setAddress(entity.getAddress());
        currentEmployee.setContractType(entity.getContractType());
        currentEmployee.setSecuNumber(entity.getSecuNumber());
        currentEmployee.setEmail(entity.getEmail());
        currentEmployee.setPersonalPhoneNumber(entity.getPersonalPhoneNumber());
        currentEmployee.setJobType(entity.getJobType());
        currentEmployee.setForeignWorker(entity.isForeignWorker());
        currentEmployee.setExitDate(entity.getExitDate());
        currentEmployee.setPhoneNumber(entity.getPhoneNumber());

        return employeeRepository.save(currentEmployee);
    }

    @Override
    public Employee updateIsActive(Long id, Employee entity) {
        Employee currentEmployee = employeeRepository.findById(id).orElseThrow(() ->
                new EntityNotFoundException("Aucun employé ne possède l'id : " + id));

        Active currentActive = currentEmployee.getActive();
        currentActive.setActive(entity.getActive().isActive());
        currentEmployee.setActive(currentActive);

        return employeeRepository.save(currentEmployee);
    }

    @Override
    public List<EmployeeDTO> getAllEmployeeDTOs() {
        List<Employee> employees = employeeRepository.findAll();
        return employees.stream()
                .map(employeeMapper::employeeToDTO)
                .collect(Collectors.toList());
    }

}
