package eu.adjoint.fibernet.employee.services;

import eu.adjoint.fibernet.employee.models.entities.Employee;
import eu.adjoint.fibernet.employee.models.entities.EmployeeDTO;
import eu.adjoint.fibernet.shared.services.BaseService;

import java.util.List;

public interface EmployeeService extends BaseService<Employee, Long> {

    Employee updateIsActive(Long id, Employee employee);

    List<EmployeeDTO> getAllEmployeeDTOs();
}
