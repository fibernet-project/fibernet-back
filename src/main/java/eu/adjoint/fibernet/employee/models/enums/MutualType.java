package eu.adjoint.fibernet.employee.models.enums;

import lombok.Getter;

@Getter
public enum MutualType {
    INDIVIDUAL("Individuelle"),
    FAMILY("Familiale");

    private final String frenchRepresentation;

    MutualType(String frenchRepresentation) {
        this.frenchRepresentation = frenchRepresentation;
    }

}

