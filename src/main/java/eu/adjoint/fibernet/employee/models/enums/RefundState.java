package eu.adjoint.fibernet.employee.models.enums;

import lombok.Getter;

@Getter
public enum RefundState {
    ON_GOING("En cours"),
    COMPLETED("Effectué");

    private final String frenchRepresentation;
    RefundState(String frenchRepresentation) {
        this.frenchRepresentation = frenchRepresentation;
    }

}

