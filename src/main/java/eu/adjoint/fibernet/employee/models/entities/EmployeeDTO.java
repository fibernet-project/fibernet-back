package eu.adjoint.fibernet.employee.models.entities;

import lombok.Data;

@Data
public class EmployeeDTO {

    private Long id;
    private String firstName;
    private String lastName;
}
