package eu.adjoint.fibernet.employee.models.entities;

import com.fasterxml.jackson.annotation.*;
import eu.adjoint.fibernet.employee.models.enums.ContractType;
import eu.adjoint.fibernet.employee.models.enums.MutualType;
import eu.adjoint.fibernet.shared.models.entities.Address;
import eu.adjoint.fibernet.deposit.models.entities.Deposit;
import eu.adjoint.fibernet.refund.models.entities.Refund;
import eu.adjoint.fibernet.shared.models.entities.Worker;
import eu.adjoint.fibernet.warning.models.entities.Warning;
import jakarta.persistence.*;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
@JsonIgnoreProperties({"deposits", "refunds", "warnings"})
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "employee")
@Data
public class Employee extends Worker implements Serializable {

    private Date entryDate;
    private boolean hasCompanyMutual;
    @Enumerated(EnumType.STRING)
    private MutualType mutualType;

    @Embedded
    private Address address;

    @OneToMany(mappedBy = "employee", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Deposit> deposits;

    @OneToMany(mappedBy = "employee", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Refund> refunds;

    @OneToMany(mappedBy = "employee", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Warning> warnings;

    @Enumerated(EnumType.STRING)
    private ContractType contractType;
    private String secuNumber;

    // String to be able to contain special characters
    private String personalPhoneNumber;
    private String jobType;
    private Date exitDate;
}

