package eu.adjoint.fibernet.employee.models.entities;

import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface EmployeeMapper {

    EmployeeDTO employeeToDTO(Employee employee);
}
