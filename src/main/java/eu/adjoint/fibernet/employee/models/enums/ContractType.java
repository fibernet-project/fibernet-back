package eu.adjoint.fibernet.employee.models.enums;

import lombok.Getter;

@Getter
public enum ContractType {
    PERMANENT_CONTRACT("CDI"),
    FIXED_TERM_CONTRACT("CDD"),
    INTERIM("Interim"),
    INTERNSHIP("Stage");

    private final String frenchRepresentation;

    ContractType(String frenchRepresentation) {
        this.frenchRepresentation = frenchRepresentation;
    }

}
