package eu.adjoint.fibernet.employee.models.repositories;

import eu.adjoint.fibernet.employee.models.entities.Employee;
import eu.adjoint.fibernet.shared.models.repositories.BaseRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface EmployeeRepository extends BaseRepository<Employee, Long> {
    // @Query("SELECT e FROM #{#entityName} e WHERE e.lastName LIKE %:query% OR e.firstName LIKE %:query%")
    //    List<Employee> search(@Param("query") String query);
}
