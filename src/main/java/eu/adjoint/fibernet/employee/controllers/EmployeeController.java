package eu.adjoint.fibernet.employee.controllers;

import eu.adjoint.fibernet.employee.models.entities.Employee;
import eu.adjoint.fibernet.employee.models.entities.EmployeeDTO;
import eu.adjoint.fibernet.employee.services.EmployeeService;
import eu.adjoint.fibernet.shared.controllers.BaseController;
import eu.adjoint.fibernet.shared.services.BaseService;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/employees")
public class EmployeeController extends BaseController<Employee, Long> {

    private final EmployeeService employeeService;

    public EmployeeController(BaseService<Employee, Long> service, EmployeeService employeeService) {
        super(service);
        this.employeeService = employeeService;
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasAnyAuthority('SCOPE_RH', 'SCOPE_ADMIN', 'SCOPE_MANAGER')")
    public ResponseEntity<Employee> update(@PathVariable Long id, @RequestBody Employee employeeDetails) {
        try {

            employeeService.update(id, employeeDetails);

            return ResponseEntity.ok(employeeDetails);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/{id}/isActive")
    @PreAuthorize("hasAnyAuthority('SCOPE_RH', 'SCOPE_ADMIN', 'SCOPE_MANAGER')")
    public ResponseEntity<Employee> updateIsActive(@PathVariable Long id, @RequestBody Employee employeeDetails) {
        try {
            var existingEmployee = employeeService.getById(id)
                    .orElseThrow(() -> new EntityNotFoundException("Employee not exist with id: " + id));

            existingEmployee.setActive(employeeDetails.getActive());

            employeeService.updateIsActive(id, existingEmployee);

            return ResponseEntity.ok(existingEmployee);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/dto")
    @PreAuthorize("hasAnyAuthority('SCOPE_RH', 'SCOPE_ADMIN', 'SCOPE_MANAGER')")
    public ResponseEntity<List<EmployeeDTO>> getAllEmployeesDTOs() {
        List<EmployeeDTO> employeeDTOs = employeeService.getAllEmployeeDTOs();
        return ResponseEntity.ok(employeeDTOs);
    }

}
