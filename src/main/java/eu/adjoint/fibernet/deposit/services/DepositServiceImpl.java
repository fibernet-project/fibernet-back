package eu.adjoint.fibernet.deposit.services;

import eu.adjoint.fibernet.deposit.models.entities.Deposit;
import eu.adjoint.fibernet.deposit.models.repositories.DepositRepository;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Transactional
@Service
public class DepositServiceImpl implements DepositService {

    private final DepositRepository depositRepository;

    public DepositServiceImpl(DepositRepository depositRepository) {
        this.depositRepository = depositRepository;
    }

    @Override
    public List<Deposit> getAll() {
        return depositRepository.findAll();
    }

    @Override
    public Optional<Deposit> getById(Long id) {
        return depositRepository.findById(id);
    }

    @Override
    public Deposit save(Deposit entity) {
        return depositRepository.save(entity);
    }

    @Override
    public void deleteById(Long id) {
        depositRepository.deleteById(id);
    }

    @Override
    public Deposit update(Long id, Deposit entity) {
        Deposit currentDeposit = depositRepository.findById(id).orElseThrow(() ->
                new EntityNotFoundException("Aucun Acompte ne possède l'id : " + id));

        currentDeposit.setDate(entity.getDate());
        currentDeposit.setAmount(entity.getAmount());
        currentDeposit.setEmployee(entity.getEmployee());

        return depositRepository.save(currentDeposit);
    }
}
