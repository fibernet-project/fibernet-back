package eu.adjoint.fibernet.deposit.services;

import eu.adjoint.fibernet.deposit.models.entities.Deposit;
import eu.adjoint.fibernet.shared.services.BaseService;

public interface DepositService extends BaseService<Deposit, Long> {

}
