package eu.adjoint.fibernet.deposit.controllers;

import eu.adjoint.fibernet.deposit.models.entities.Deposit;
import eu.adjoint.fibernet.deposit.services.DepositService;
import eu.adjoint.fibernet.shared.controllers.BaseController;
import eu.adjoint.fibernet.shared.services.BaseService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/deposits")
public class DepositController extends BaseController<Deposit, Long> {

    private final DepositService depositService;
    public DepositController(BaseService<Deposit, Long> service, DepositService depositService) {
        super(service);
        this.depositService = depositService;
    }

    @PutMapping("/{id}")
    public ResponseEntity<Deposit> updateDeposit(@PathVariable Long id, @RequestBody Deposit depositDetails) {
        try {

            depositService.update(id, depositDetails);
            return ResponseEntity.ok(depositDetails);

        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
