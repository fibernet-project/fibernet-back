package eu.adjoint.fibernet.deposit.models.repositories;

import eu.adjoint.fibernet.deposit.models.entities.Deposit;
import eu.adjoint.fibernet.shared.models.repositories.BaseRepository;

public interface DepositRepository extends BaseRepository<Deposit, Long> {
}
