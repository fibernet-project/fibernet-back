package eu.adjoint.fibernet.deposit.models.entities;

import eu.adjoint.fibernet.shared.models.entities.Operations;
import jakarta.persistence.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;

@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "deposit")
@Data
public class Deposit extends Operations implements Serializable {

    private double amount;
}
