package eu.adjoint.fibernet.appUser.controllers;

import eu.adjoint.fibernet.shared.controllers.BaseController;
import eu.adjoint.fibernet.appUser.models.entities.AppUser;
import eu.adjoint.fibernet.appUser.services.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/users")
public class UserController extends BaseController<AppUser, Long> {

    private final UserService userService;

    public UserController(UserService userService) {
        super(userService);
        this.userService = userService;
    }

    @GetMapping("/api/{username}")
    @PreAuthorize("hasAnyAuthority('SCOPE_ADMIN')")
    public ResponseEntity<AppUser> findUserByUsername(@PathVariable String username) {
        try {
            AppUser appUserToFind = userService.findByUsername(username);
            if (appUserToFind != null) {
                return ResponseEntity.ok(appUserToFind);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/create")
    @PreAuthorize("hasAnyAuthority('SCOPE_ADMIN')")
    public ResponseEntity<AppUser> createUser(@RequestBody AppUser appUser) {
        try {
            userService.createUser(appUser.getUsername(), appUser.getPassword(), appUser.getRoles(), appUser.getFirstName(),
                    appUser.getLastName(), appUser.getEmail());
            return ResponseEntity.ok(appUser);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasAnyAuthority('SCOPE_ADMIN')")
    public ResponseEntity<AppUser> update(@PathVariable long id, @RequestBody AppUser userDetails) {
        try {

            userService.update(id, userDetails);

            return ResponseEntity.ok(userDetails);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/change/{username}")
    public ResponseEntity<AppUser> updatePassword(@RequestBody String password, @PathVariable String username) {
        try {
            AppUser userToFind = userService.updatePassword(username, password);
            return ResponseEntity.ok(userToFind);

        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/forgot-password")
    public ResponseEntity<String> forgotPassword(@RequestParam String username) {
        try {
            userService.sendPasswordResetEmail(username);
            return ResponseEntity.ok("Un e-mail de réinitialisation de mot de passe a été envoyé" +
                    " à l'adresse e-mail associée à ce nom d'utilisateur.");
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Erreur lors de l'envoi de l'e-mail de " +
                    "réinitialisation de mot de passe.");
        }
    }

}

