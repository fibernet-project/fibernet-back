package eu.adjoint.fibernet.appUser.services;

import eu.adjoint.fibernet.appUser.models.entities.AppUser;
import eu.adjoint.fibernet.appUser.models.enums.Roles;
import eu.adjoint.fibernet.appUser.models.repositories.UserRepository;
import eu.adjoint.fibernet.shared.models.entities.Email;
import eu.adjoint.fibernet.shared.services.EmailSenderService;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import static eu.adjoint.fibernet.appUser.models.entities.PasswordGenerator.generatePassword;

@Service
@Transactional
public class UserServiceImpl implements UserService, UserDetailsService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    private final EmailSenderService emailSenderService;

    public UserServiceImpl(UserRepository userRepository, PasswordEncoder passwordEncoder, EmailSenderService emailSenderService) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.emailSenderService = emailSenderService;
    }

    @Override
    public AppUser createUser(String username, String password, Set<Roles> roles, String firstName, String lastName, Email email) {
        AppUser userToFind = findByUsername(username);
        if (userToFind != null) {
            throw new IllegalArgumentException("Ce nom d'utilisateur existe déja");
        }
        AppUser appUserCreated = new AppUser();
        appUserCreated.setUsername(username);
        appUserCreated.setPassword(passwordEncoder.encode(password));
        appUserCreated.setRoles(roles);
        appUserCreated.setFirstName(firstName);
        appUserCreated.setLastName(lastName);
        appUserCreated.setEmail(email);

        return userRepository.save(appUserCreated);
    }

    @Override
    public AppUser findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        AppUser appUser = userRepository.findByUsername(username);

        if (appUser == null) {
            throw new UsernameNotFoundException("Utilisateur non trouvé avec le nom d'utilisateur : " + username);
        }

        return User.builder()
                .username(appUser.getUsername())
                .password(appUser.getPassword())
                .roles(String.valueOf(appUser.getRoles()))
                .build();
    }

    @Override
    public List<AppUser> getAll() {
        return userRepository.findAll();
    }

    @Override
    public Optional<AppUser> getById(Long id) {
        return userRepository.findById(id);
    }

    @Override
    public AppUser save(AppUser entity) {
        return userRepository.save(entity);
    }

    @Override
    public void deleteById(Long id) {
        userRepository.deleteById(id);
    }

    @Override
    public AppUser update(Long id, AppUser entity) {
        AppUser currentUser = userRepository.findById(id).orElseThrow(() ->
                new EntityNotFoundException("Aucun utilisateur ne possède l'id : " + id));

        currentUser.setLastName(entity.getLastName());
        currentUser.setFirstName(entity.getFirstName());
        currentUser.setRoles(entity.getRoles());
        currentUser.setUsername(entity.getUsername());
        currentUser.setEmail(entity.getEmail());


        return userRepository.save(currentUser);
    }

    @Override
    public AppUser updatePassword(String username, String password) {

        AppUser userToUpdate = userRepository.findByUsername(username);

        if (userToUpdate == null) {
            throw new EntityNotFoundException("Utilisateur non trouvé avec le nom d'utilisateur : " + username);
        }

        userToUpdate.setPassword(passwordEncoder.encode(password));

        return userRepository.save(userToUpdate);
    }

    @Override
    public void sendPasswordToEmail(String username) {
        AppUser user = userRepository.findByUsername(username);

        if (user == null) {
            throw new EntityNotFoundException("Utilisateur non trouvé avec le nom d'utilisateur : " + username);
        }

        sendNotificationEmail(user.getEmail().getEmail(), user.getPassword());
    }

    @Override
    public void sendPasswordResetEmail(String username) {
        AppUser user = userRepository.findByUsername(username);

        if (user == null) {
            throw new EntityNotFoundException("Utilisateur non trouvé avec le nom d'utilisateur : " + username);
        }

        String plainTextPassword = generatePassword(8);

        user.setPassword(passwordEncoder.encode(plainTextPassword));
        userRepository.save(user);

        sendNotificationEmail(user.getEmail().getEmail(), plainTextPassword);
    }

    @Override
    public void sendNotificationEmail(String email, String password) {
        System.out.println("Sending password to email.");
        emailSenderService.sendEmail(email, "Réinitialisation du mot de passe",
                "Bonjour, \n\n" +
                        "Suite à votre demande de réinitialisation,\n\n" +
                        "Voici votre nouveau mot de passe : " + password + "\n\n" +
                        "Merci de le changer au plus vite\n\n" +
                        "Bien cordialement,\n\n" +
                        "L'équipe technique d'Adjoint");
    }
    }