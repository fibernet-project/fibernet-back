package eu.adjoint.fibernet.appUser.services;

import eu.adjoint.fibernet.appUser.models.enums.Roles;
import eu.adjoint.fibernet.shared.models.entities.Email;
import eu.adjoint.fibernet.shared.services.BaseService;
import eu.adjoint.fibernet.appUser.models.entities.AppUser;

import java.util.Set;

public interface UserService extends BaseService<AppUser, Long> {

    AppUser createUser(String username, String password, Set<Roles> roles, String firstName, String lastName, Email email);

    AppUser findByUsername(String username);

    AppUser updatePassword(String username, String password);

    void sendPasswordToEmail(String username);

    void sendPasswordResetEmail(String username);

    void sendNotificationEmail(String email, String password);
}
