package eu.adjoint.fibernet.appUser.models.enums;

public enum Roles {
    ADMIN,
    MANAGER,
    RH,
    USER
}
