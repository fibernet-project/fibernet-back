package eu.adjoint.fibernet.appUser.models.repositories;

import eu.adjoint.fibernet.shared.models.repositories.BaseRepository;
import eu.adjoint.fibernet.appUser.models.entities.AppUser;

public interface UserRepository extends BaseRepository<AppUser, Long> {

    AppUser findByUsername(String username);
}
