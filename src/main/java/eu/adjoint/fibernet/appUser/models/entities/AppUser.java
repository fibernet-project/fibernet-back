package eu.adjoint.fibernet.appUser.models.entities;

import eu.adjoint.fibernet.appUser.models.enums.Roles;
import eu.adjoint.fibernet.shared.models.entities.Person;
import jakarta.persistence.*;
import jakarta.validation.constraints.Pattern;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "appUser")
public class AppUser extends Person implements Serializable {

    @Column(unique = true)
    private String username;

    @Pattern(regexp = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[@#$%^&+=])(?=\\S+$).{8,}$",
            message = "Le mot de passe doit contenir au moins 8 caractères avec au moins une majuscule, " +
                    "une minuscule, un chiffre et un caractère spécial.")
    private String password;

    @Enumerated(EnumType.STRING)
    @ElementCollection(targetClass = Roles.class)
    @CollectionTable(name = "app_user_roles", joinColumns = @JoinColumn(name = "app_user_id"))
    @Column(name = "role")
    private Set<Roles> roles = new HashSet<>();
}
