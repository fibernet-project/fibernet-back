package eu.adjoint.fibernet;

import eu.adjoint.fibernet.appUser.models.entities.AppUser;
import eu.adjoint.fibernet.appUser.models.enums.Roles;
import eu.adjoint.fibernet.appUser.models.repositories.UserRepository;
import eu.adjoint.fibernet.shared.models.entities.Email;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.HashSet;
import java.util.Set;

@SpringBootApplication
@EnableScheduling
public class FibernetApplication implements CommandLineRunner {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private PasswordEncoder passwordEncoder;

	public static void main(String[] args) {
		SpringApplication.run(FibernetApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

		addSampleUsers();
	}

	private void addSampleUsers() {
		if (userRepository.findByUsername("user31") == null) {
			AppUser appUser = new AppUser();
			appUser.setUsername("user31");
			appUser.setPassword(passwordEncoder.encode("Adjoint@@23"));
			Set<Roles> userRoles = new HashSet<>();
			userRoles.add(Roles.USER);
			appUser.setRoles(userRoles);
			appUser.setFirstName("Jean");
			appUser.setLastName("Jacques");
			Email email = new Email();
			email.setEmail("grokifri31100@hotmail.fr");
			appUser.setEmail(email);
			userRepository.save(appUser);
		}

		if (userRepository.findByUsername("admin31") == null) {
			AppUser appUser = new AppUser();
			appUser.setUsername("admin31");
			appUser.setPassword(passwordEncoder.encode("Adjoint@@23"));
			Set<Roles> adminRoles = new HashSet<>();
			adminRoles.add(Roles.ADMIN);
			adminRoles.add(Roles.USER);
			appUser.setFirstName("Charles");
			appUser.setLastName("Péguy");
			Email email = new Email();
			email.setEmail("guermoud.salih@gmail.com");
			appUser.setEmail(email);
			appUser.setRoles(adminRoles);
			userRepository.save(appUser);
		}
	}

}
