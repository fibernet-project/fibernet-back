package eu.adjoint.fibernet.refund.controllers;

import eu.adjoint.fibernet.refund.models.entities.Refund;
import eu.adjoint.fibernet.refund.services.RefundService;
import eu.adjoint.fibernet.shared.controllers.BaseController;
import eu.adjoint.fibernet.shared.services.BaseService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/refunds")
public class RefundController extends BaseController<Refund, Long> {

    private final RefundService refundService;

    public RefundController(BaseService<Refund, Long> service, RefundService refundService) {
        super(service);
        this.refundService = refundService;
    }

    @PutMapping("/{id}")
    public ResponseEntity<Refund> updateRefund(@PathVariable Long id, @RequestBody Refund refundDetails) {
        try {
            refundService.update(id, refundDetails);
            return ResponseEntity.ok(refundDetails);

        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
