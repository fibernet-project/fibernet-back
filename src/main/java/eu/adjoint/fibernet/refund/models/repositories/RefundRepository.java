package eu.adjoint.fibernet.refund.models.repositories;

import eu.adjoint.fibernet.refund.models.entities.Refund;
import eu.adjoint.fibernet.shared.models.repositories.BaseRepository;

public interface RefundRepository extends BaseRepository<Refund, Long> {
}
