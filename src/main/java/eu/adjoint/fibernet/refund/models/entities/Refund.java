package eu.adjoint.fibernet.refund.models.entities;

import eu.adjoint.fibernet.employee.models.enums.RefundState;
import eu.adjoint.fibernet.shared.models.entities.Operations;
import jakarta.persistence.*;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "refund")
@Data
public class Refund extends Operations implements Serializable {

    private double totalRefund;
    private RefundState refundState;
}
