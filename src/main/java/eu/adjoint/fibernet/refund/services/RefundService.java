package eu.adjoint.fibernet.refund.services;

import eu.adjoint.fibernet.refund.models.entities.Refund;
import eu.adjoint.fibernet.shared.services.BaseService;

public interface RefundService extends BaseService<Refund, Long> {
}
