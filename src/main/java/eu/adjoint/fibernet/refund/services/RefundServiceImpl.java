package eu.adjoint.fibernet.refund.services;

import eu.adjoint.fibernet.refund.models.entities.Refund;
import eu.adjoint.fibernet.refund.models.repositories.RefundRepository;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
@Transactional
@Service
public class RefundServiceImpl implements RefundService {

    private final RefundRepository refundRepository;

    public RefundServiceImpl(RefundRepository refundRepository) {
        this.refundRepository = refundRepository;
    }

    @Override
    public List<Refund> getAll() {
        return refundRepository.findAll();
    }

    @Override
    public Optional<Refund> getById(Long id) {
        return refundRepository.findById(id);
    }

    @Override
    public Refund save(Refund entity) {
        return refundRepository.save(entity);
    }

    @Override
    public void deleteById(Long id) {
        refundRepository.deleteById(id);
    }

    @Override
    public Refund update(Long id, Refund entity) {
        Refund currentRefund = refundRepository.findById(id).orElseThrow(() ->
                new EntityNotFoundException("Aucun remboursement ne possède l'id :" + id));

        currentRefund.setRefundState(entity.getRefundState());

        return refundRepository.save(currentRefund);
    }
}
