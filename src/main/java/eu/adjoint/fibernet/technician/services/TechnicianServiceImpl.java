package eu.adjoint.fibernet.technician.services;

import eu.adjoint.fibernet.shared.models.entities.Active;
import eu.adjoint.fibernet.technician.models.entities.Technician;
import eu.adjoint.fibernet.technician.models.repositories.TechnicianRepository;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class TechnicianServiceImpl implements TechnicianService {

    private final TechnicianRepository technicianRepository;

    public TechnicianServiceImpl(TechnicianRepository technicianRepository) {
        this.technicianRepository = technicianRepository;
    }

    @Override
    public List<Technician> getAll() {
        return technicianRepository.findAll();
    }

    @Override
    public Optional<Technician> getById(Long id) {
        return technicianRepository.findById(id);
    }

    @Override
    public Technician save(Technician entity) {
        return technicianRepository.save(entity);
    }

    @Override
    public void deleteById(Long id) {
        technicianRepository.deleteById(id);
    }

    @Override
    public Technician update(Long id, Technician entity) {

        Technician currentTechnician = technicianRepository.findById(id).orElseThrow(() ->
                new EntityNotFoundException("Aucun technicien ne possède l'id : " + id));

        currentTechnician.setLastName(entity.getLastName());
        currentTechnician.setFirstName(entity.getFirstName());
        currentTechnician.setBirthDate(entity.getBirthDate());
        currentTechnician.setIdTechnician(entity.getIdTechnician());
        currentTechnician.setLogin(entity.getLogin());
        currentTechnician.setPassword(entity.getPassword());
        currentTechnician.setArea(entity.getArea());
        currentTechnician.setContractor(entity.getContractor());
        currentTechnician.setProductionStart(entity.getProductionStart());
        currentTechnician.setProductionEnd(entity.getProductionEnd());
        currentTechnician.setComment(entity.getComment());
        currentTechnician.setEmail(entity.getEmail());
        currentTechnician.setHasDPAE(entity.isHasDPAE());
        currentTechnician.setPhoneNumber(entity.getPhoneNumber());

        return technicianRepository.save(currentTechnician);
    }

    @Override
    public Technician updateIsActive(Long id, Technician entity) {
        Technician currentTechnician = technicianRepository.findById(id).orElseThrow(() ->
                new EntityNotFoundException("Aucun technicien ne possède l'id : " + id));

        Active currentActive = currentTechnician.getActive();
        currentActive.setActive(entity.getActive().isActive());
        currentTechnician.setActive(currentActive);

        return technicianRepository.save(currentTechnician);
    }


    @Override
    public Technician updateAuditTechnician(Long id, Technician entity) {
        Technician currentTechnician = technicianRepository.findById(id).orElseThrow(() ->
                new EntityNotFoundException("Aucun technicien ne possède l'id : " + id));

        currentTechnician.setAuditInformations(entity.getAuditInformations());

        return technicianRepository.save(currentTechnician);
    }

}
