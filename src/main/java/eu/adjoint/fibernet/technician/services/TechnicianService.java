package eu.adjoint.fibernet.technician.services;

import eu.adjoint.fibernet.shared.services.BaseService;
import eu.adjoint.fibernet.technician.models.entities.Technician;

public interface TechnicianService extends BaseService<Technician, Long> {

    Technician updateIsActive(Long is, Technician technician);
    Technician updateAuditTechnician(Long id, Technician technician);

}
