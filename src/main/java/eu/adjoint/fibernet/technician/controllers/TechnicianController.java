package eu.adjoint.fibernet.technician.controllers;

import eu.adjoint.fibernet.shared.controllers.BaseController;
import eu.adjoint.fibernet.shared.services.BaseService;
import eu.adjoint.fibernet.technician.models.entities.Technician;
import eu.adjoint.fibernet.technician.services.TechnicianService;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/technicians")
public class TechnicianController extends BaseController<Technician, Long> {

    private final TechnicianService technicianService;

    public TechnicianController(BaseService<Technician, Long> service, TechnicianService technicianService) {
        super(service);
        this.technicianService = technicianService;
    }

    @PutMapping("/{id}")
    public ResponseEntity<Technician> updateTechnician(@PathVariable Long id, @RequestBody Technician technicianDetails) {
        try {

            technicianService.update(id, technicianDetails);

            return ResponseEntity.ok(technicianDetails);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/{id}/audit")
    public ResponseEntity<Technician> updateAuditTechnician(@PathVariable Long id, @RequestBody Technician technicianDetails) {
        try {
            Technician existingTechnician = technicianService.getById(id)
                    .orElseThrow(() -> new EntityNotFoundException("Technician not found with id: " + id));

            existingTechnician.setAuditInformations(technicianDetails.getAuditInformations());

            technicianService.update(id, existingTechnician);

            return ResponseEntity.ok(existingTechnician);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/{id}/isActive")
    public ResponseEntity<Technician> updateIsActive(@PathVariable Long id, @RequestBody Technician technicianDetails) {
        try {
            Technician existingTechnician = technicianService.getById(id)
                    .orElseThrow(() -> new EntityNotFoundException("Technician not found with id: " + id));

            existingTechnician.setActive(technicianDetails.getActive());

            technicianService.update(id, existingTechnician);

            return ResponseEntity.ok(existingTechnician);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
