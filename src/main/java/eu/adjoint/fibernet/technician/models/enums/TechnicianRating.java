package eu.adjoint.fibernet.technician.models.enums;

public enum TechnicianRating {
    GOOD,
    AVERAGE,
    BAD
}
