package eu.adjoint.fibernet.technician.models.entities;

import eu.adjoint.fibernet.technician.models.enums.TechnicianRating;
import jakarta.persistence.Embeddable;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Data;

import java.io.Serializable;

@Embeddable
@Data
public class AuditInformations implements Serializable {

    private String auditComment;
    private boolean professionalCard;
    private boolean qualifications;

    // AIPR: Authorization for Intervention in the Proximity of Networks
    private boolean aiprAuthorization;

    // PPR: Prevention Plan for Networks
    private boolean pprPlan;
    private boolean vehicleMarking;
    private boolean personalProtectiveEquipment;
    private boolean equipment;
    private boolean workMethodology;
    private boolean process;

    @Enumerated(EnumType.STRING)
    private TechnicianRating technicianRating;
    private boolean hasCherryPicker;

    // CACES: Certificate of Aptitude for Safe Driving
    private boolean hasCACES;

    // VGP : Periodic General Inspection
    private boolean hasVGP;

    // Indicates whether there is a flying cherry picker in the team.
    private boolean hasFlyingCherryPicker;
}
