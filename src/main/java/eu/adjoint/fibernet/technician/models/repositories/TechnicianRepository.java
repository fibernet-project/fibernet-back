package eu.adjoint.fibernet.technician.models.repositories;

import eu.adjoint.fibernet.shared.models.repositories.BaseRepository;
import eu.adjoint.fibernet.technician.models.entities.Technician;

public interface TechnicianRepository extends BaseRepository<Technician, Long> {
}
