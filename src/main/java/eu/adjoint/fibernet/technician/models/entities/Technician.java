package eu.adjoint.fibernet.technician.models.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import eu.adjoint.fibernet.contractor.models.entities.Contractor;
import eu.adjoint.fibernet.shared.models.entities.Worker;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.lang.NonNull;

import java.io.Serializable;
import java.util.Date;

@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "technician")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Technician extends Worker implements Serializable {

    private Long idTechnician;
    private String login;
    private String password;
    private int area;

    @ManyToOne
    @JoinColumn(name = "contractor_id")
    @NonNull
    @JsonIgnoreProperties({"technicians", "contractorInformations", "address"})
    private Contractor contractor;
    private Date productionStart;
    private Date productionEnd;
    private String comment;

    @Embedded
    private AuditInformations auditInformations = new AuditInformations();

    // DPAE : The pre-employment declaration
    private boolean hasDPAE;
}

