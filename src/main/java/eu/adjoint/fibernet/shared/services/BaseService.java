package eu.adjoint.fibernet.shared.services;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

public interface BaseService <T, ID extends Serializable>{

    List<T> getAll();

    Optional<T> getById(ID id);

    T save(T entity);

    void deleteById(ID id);

    T update(ID id, T entity);

}
