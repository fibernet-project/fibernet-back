package eu.adjoint.fibernet.shared.models.entities;

import jakarta.persistence.Embeddable;
import lombok.Data;

@Data
@Embeddable
public class PhoneNumber {

    // String to be able to contain special characters
    private String phoneNumber;
}
