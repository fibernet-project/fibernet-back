package eu.adjoint.fibernet.shared.models.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import eu.adjoint.fibernet.employee.models.entities.Employee;
import jakarta.persistence.*;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

@EqualsAndHashCode(callSuper = true)
@Data
@MappedSuperclass
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class Operations extends BaseEntity implements Serializable {

    private Date date;

    @ManyToOne
    @JoinColumn(name = "employee_id")
    @JsonIgnoreProperties({"deposits", "refunds", "warnings"})
    private Employee employee;
}
