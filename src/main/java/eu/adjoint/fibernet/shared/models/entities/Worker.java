package eu.adjoint.fibernet.shared.models.entities;

import jakarta.persistence.*;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;
@EqualsAndHashCode(callSuper = true)
@Data
@MappedSuperclass
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class Worker extends Person implements Serializable {

    private Date birthDate;
    private boolean foreignWorker;

    @Embedded
    private Active active;

    @Embedded
    private PhoneNumber phoneNumber;
}
