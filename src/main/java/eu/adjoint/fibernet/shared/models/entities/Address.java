package eu.adjoint.fibernet.shared.models.entities;

import jakarta.persistence.Embeddable;
import lombok.Data;

import java.io.Serializable;
@Embeddable
@Data
public class Address implements Serializable {

    private String contentAddress;
    private String city;
    private int zipCode;
    private String country;
}
