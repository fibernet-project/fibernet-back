package eu.adjoint.fibernet.shared.models.entities;

import jakarta.persistence.Embeddable;
import lombok.Data;

import java.io.Serializable;

@Data
@Embeddable
public class Email implements Serializable {
    private String email;
}
