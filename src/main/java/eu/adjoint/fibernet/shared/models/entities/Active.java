package eu.adjoint.fibernet.shared.models.entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.persistence.Embeddable;
import lombok.Data;

@Data
@Embeddable
public class Active {

    @JsonProperty("isActive")
    private boolean isActive;
}
