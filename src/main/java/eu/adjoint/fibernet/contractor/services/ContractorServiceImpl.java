package eu.adjoint.fibernet.contractor.services;

import eu.adjoint.fibernet.contractor.models.entities.Contractor;
import eu.adjoint.fibernet.contractor.models.entities.ContractorInformations;
import eu.adjoint.fibernet.contractor.models.repositories.ContractorRepository;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class ContractorServiceImpl implements ContractorService {

    private final ContractorRepository contractorRepository;

    public ContractorServiceImpl(ContractorRepository contractorRepository) {
        this.contractorRepository = contractorRepository;
    }

    @Override
    public List<Contractor> getAll() {
        return contractorRepository.findAll();
    }

    @Override
    public Optional<Contractor> getById(Long id) {
        return contractorRepository.findById(id);
    }

    @Override
    public Contractor save(Contractor contractor) {
        if (isValidContractor(contractor)) {
            return contractorRepository.save(contractor);
        } else {
            throw new IllegalArgumentException("Une ou plusieurs dates ne sont pas valides");
        }
    }

    @Override
    public void deleteById(Long id) {
        contractorRepository.deleteById(id);
    }

    @Override
    public boolean isValidContractor(Contractor contractor) {
        ContractorInformations contractorInformations = contractor.getContractorInformations();
        if (contractorInformations == null) {
            return false;
        }
        return contractorInformations.isValidKBisDateRange() &&
                contractorInformations.isValidUrssafDateRange() &&
                contractorInformations.isValidDecennaleDateRange() &&
                contractorInformations.isValidForeignWorkersDateRange() &&
                contractorInformations.isValidTaxRegularityCertificateDateRange();
    }

    @Override
    public Contractor update(Long id, Contractor updatedContractor) {
        Contractor currentContractor = contractorRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Aucun prestataire ne possède l'id : " + id));
        updateContractorFields(currentContractor, updatedContractor);
        return contractorRepository.save(currentContractor);
    }

    @Override
    public Contractor updateIsActive(Long id, Contractor updatedContractor) {
        Contractor currentContractor = contractorRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Aucun prestataire ne possède l'id : " + id));
        updateContractorFields(currentContractor, updatedContractor);
        currentContractor.setActive(updatedContractor.getActive());
        return contractorRepository.save(currentContractor);
    }

    private void updateContractorFields(Contractor currentContractor, Contractor updatedContractor) {
        currentContractor.setName(updatedContractor.getName());
        currentContractor.setResponsibleName(updatedContractor.getResponsibleName());
        currentContractor.setTechnicians(updatedContractor.getTechnicians());
        currentContractor.setAddress(updatedContractor.getAddress());
        currentContractor.setContractorInformations(updatedContractor.getContractorInformations());
        currentContractor.setSirenNumber(updatedContractor.getSirenNumber());
        currentContractor.setEmail(updatedContractor.getEmail());
        currentContractor.setPhoneNumber(updatedContractor.getPhoneNumber());
    }
}
