package eu.adjoint.fibernet.contractor.services;

import eu.adjoint.fibernet.contractor.models.entities.Contractor;
import eu.adjoint.fibernet.shared.services.BaseService;

public interface ContractorService extends BaseService<Contractor, Long> {
    boolean isValidContractor(Contractor contractor);

    Contractor updateIsActive(Long id, Contractor contractor);
}
