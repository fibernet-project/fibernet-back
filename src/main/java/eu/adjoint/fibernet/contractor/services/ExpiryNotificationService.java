package eu.adjoint.fibernet.contractor.services;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import eu.adjoint.fibernet.contractor.models.entities.Contractor;
import eu.adjoint.fibernet.contractor.models.entities.ContractorInformations;
import eu.adjoint.fibernet.contractor.models.repositories.ContractorRepository;
import eu.adjoint.fibernet.shared.services.EmailSenderService;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
public class ExpiryNotificationService {

    private final EmailSenderService emailSenderService;

    private final ContractorRepository contractorRepository;

    public ExpiryNotificationService(EmailSenderService emailSenderService, ContractorRepository contractorRepository) {
        this.emailSenderService = emailSenderService;
        this.contractorRepository = contractorRepository;
    }

    //@Scheduled(cron = "0 * * * * ?") // Execution every minute

    @Scheduled(cron = "0 0 0 * * ?") // Execution at midnight every day
    public void sendExpiryNotifications() {

        List<Contractor> contractors = contractorRepository.findAll();

        for (Contractor contractor : contractors) {

            ContractorInformations contractorInformations = contractor.getContractorInformations();

            if (checkExpiry(contractorInformations.getDecennaleEndDate())
                    || checkExpiry(contractorInformations.getUrssafEndDate())
                    || checkExpiry(contractorInformations.getForeignWorkersEndDate())
                    || checkExpiry(contractorInformations.getTaxRegularityCertificateEnd())
                    || checkExpiry(contractorInformations.getKBisEndDate())) {

                sendNotificationEmail();
            }
        }
    }

    private boolean checkExpiry(Date endDate) {
        if (endDate != null) {

            LocalDate today = LocalDate.now();
            LocalDate expiryDateMinus30Days = LocalDate.ofInstant(endDate.toInstant(), ZoneId.systemDefault()).minusDays(30);
            return expiryDateMinus30Days.isBefore(today);

        }
        return false;
    }

    private void sendNotificationEmail() {
        System.out.println("Sending notification email.");
        emailSenderService.sendEmail("rh@adjoint.eu", "Rappel d'échéance - Renouvellement de Document Prestataire",
                "Bonjour, \n\n" +
                        "Nous vous rappelons que certains documents de prestataire expireront dans les 30 prochains jours.\n" +
                        "Merci d'initier le processus de renouvellement dès que possible.\n" +
                        "\n" +
                        "Bien cordialement,\n\n" +
                        "L'équipe technique d'Adjoint");
    }
}

