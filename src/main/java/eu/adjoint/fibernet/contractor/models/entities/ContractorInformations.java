package eu.adjoint.fibernet.contractor.models.entities;

import jakarta.persistence.Embeddable;
import jakarta.validation.constraints.AssertTrue;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Embeddable
@Data
public class ContractorInformations implements Serializable {

    private Date kbisStartDate;
    private Date kBisEndDate;
    private Date urssafStartDate;
    private Date urssafEndDate;
    private Date decennaleStartDate;
    private Date decennaleEndDate;

    // Certificate for foreign workers
    private Date foreignWorkersStartDate;
    private Date foreignWorkersEndDate;
    private Date taxRegularityCertificateStart;
    private Date taxRegularityCertificateEnd;

    @AssertTrue(message = "La date de début KBIS doit être antérieure à la date de fin KBIS")
    public boolean isValidKBisDateRange() {
        return kbisStartDate == null || kBisEndDate == null || kbisStartDate.before(kBisEndDate);
    }

    @AssertTrue(message = "La date de début URSSAF doit être antérieure à la date de fin URSSAF")
    public boolean isValidUrssafDateRange() {
        return urssafStartDate == null || urssafEndDate == null || urssafStartDate.before(urssafEndDate);
    }

    @AssertTrue(message = "La date de début décennale doit être antérieure à la date de fin décennale")
    public boolean isValidDecennaleDateRange() {
        return decennaleStartDate == null || decennaleEndDate == null || decennaleStartDate.before(decennaleEndDate);
    }

    @AssertTrue(message = "La date de début des travailleurs étrangers doit être antérieure à la date de fin des travailleurs étrangers")
    public boolean isValidForeignWorkersDateRange() {
        return foreignWorkersStartDate == null || foreignWorkersEndDate == null || foreignWorkersStartDate.before(foreignWorkersEndDate);
    }

    @AssertTrue(message = "La date de début du certificat de régularité fiscale doit être antérieure à la date de fin du certificat de régularité fiscale")
    public boolean isValidTaxRegularityCertificateDateRange() {
        return taxRegularityCertificateStart == null || taxRegularityCertificateEnd == null || taxRegularityCertificateStart.before(taxRegularityCertificateEnd);
    }
}
