package eu.adjoint.fibernet.contractor.models.repositories;

import eu.adjoint.fibernet.contractor.models.entities.Contractor;
import eu.adjoint.fibernet.shared.models.repositories.BaseRepository;

public interface ContractorRepository extends BaseRepository<Contractor, Long> {
}
