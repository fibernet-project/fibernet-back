package eu.adjoint.fibernet.contractor.models.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import eu.adjoint.fibernet.shared.models.entities.*;
import eu.adjoint.fibernet.technician.models.entities.Technician;
import jakarta.persistence.*;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.List;


@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "contractor")
public class Contractor extends BaseEntity implements Serializable {

    private String name;
    private String responsibleName;

    @OneToMany(mappedBy = "contractor")
    @JsonIgnoreProperties({"contractor"})
    private List<Technician> technicians;

    @Embedded
    private Address address;

    @Embedded
    private ContractorInformations contractorInformations;
    private Long sirenNumber;

    @Embedded
    private Active active;

    @Embedded
    private PhoneNumber phoneNumber;

    @Embedded
    private Email email;
}

