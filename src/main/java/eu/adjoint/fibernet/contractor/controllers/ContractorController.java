package eu.adjoint.fibernet.contractor.controllers;

import eu.adjoint.fibernet.contractor.models.entities.Contractor;
import eu.adjoint.fibernet.contractor.services.ContractorService;
import eu.adjoint.fibernet.shared.controllers.BaseController;
import eu.adjoint.fibernet.shared.services.BaseService;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/contractors")
public class ContractorController extends BaseController<Contractor, Long> {

    private final ContractorService contractorService;
    @Autowired
    public ContractorController(BaseService<Contractor, Long> service, ContractorService contractorService) {
        super(service);
        this.contractorService = contractorService;
    }

    @PutMapping("/{id}")
    public ResponseEntity<Contractor> updateContractor(@PathVariable long id, @RequestBody Contractor contractorDetails) {
        try {

            contractorService.update(id, contractorDetails);

            return ResponseEntity.ok(contractorDetails);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/{id}/isActive")
    public ResponseEntity<Contractor> updateIsActive(@PathVariable long id, @RequestBody Contractor contractorDetails) {
        try {
            Contractor existingContractor = contractorService.getById(id)
                    .orElseThrow(() -> new EntityNotFoundException("Contractor not exist with id : " + id));

            existingContractor.setActive(contractorDetails.getActive());

            contractorService.updateIsActive(id, existingContractor);

            return ResponseEntity.ok(existingContractor);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    }
